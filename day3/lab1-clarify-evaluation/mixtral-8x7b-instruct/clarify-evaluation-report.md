### Foundation Model Evaluations with SageMaker Clarify  

# Evaluation Report  

## Task: Question Answering  

This section shows the overall scores for each successful evaluation.   

### Q&A Accuracy  

Measures how well the model performs in question answering (Q&A) tasks.  

<table align=center style="table-layout: fixed;">  
  
<tr> <th >Dataset</th> <th style="text-align: right;">F1 Over Words Score</th> <th style="text-align: right;">Exact Match Score</th> <th style="text-align: right;">Quasi Exact Match Score</th> </tr>  
<tr> <td ><a style="color:#006DAA;" href="https://github.com/google-research-datasets/boolean-questions">BoolQ</a></td> <td style="text-align: right;">0.102604</td> <td style="text-align: right;">0.0</td> <td style="text-align: right;">0.0</td> </tr>  
<tr> <td ><a style="color:#006DAA;" href="https://github.com/google-research-datasets/natural-questions">Natural Questions</a></td> <td style="text-align: right;">0.302638</td> <td style="text-align: right;">0.13</td> <td style="text-align: right;">0.14</td> </tr>  
<tr> <td ><a style="color:#006DAA;" href="http://nlp.cs.washington.edu/triviaqa/">TriviaQA</a></td> <td style="text-align: right;">0.589976</td> <td style="text-align: right;">0.42</td> <td style="text-align: right;">0.44</td> </tr>  
</table>  

## Evaluation Job Configuration  

<table align=center >  
  
<tr> <th >Parameter</th> <th style="text-align: right;">Value</th> </tr>  
<tr> <td >Model</td> <td style="text-align: right;">huggingface-llm-mixtral-8x7b-instruct</td> </tr>  
<tr> <td >Model Type</td> <td style="text-align: right;">SageMaker Jumpstart Model</td> </tr>  
<tr> <td >Evaluation Methods</td> <td style="text-align: right;">Q&A Accuracy</td> </tr>  
<tr> <td >Datasets</td> <td style="text-align: right;">BoolQ, Natural Questions, TriviaQA</td> </tr>  
</table>  

## Detailed Evaluation Results  

Below are the selected model evaluations:  

### Q&A Accuracy  

This evaluation measures how well the model performs in question answering (QA) tasks, both in the closed-book and the open-book setting. In open-book QA the model is presented with a reference text containing the answer, i.e., the model’s task is to extract the correct answer from the text. In the closed-book case the model is not presented with any additional information but uses its own world knowledge to answer the question.  You selected to evaluate your model with open-source (<a style="color:#006DAA;" href="https://github.com/google-research-datasets/boolean-questions">BoolQ</a>, <a style="color:#006DAA;" href="https://github.com/google-research-datasets/natural-questions">Natural Questions</a>, <a style="color:#006DAA;" href="http://nlp.cs.washington.edu/triviaqa/">TriviaQA</a>) datasets.  

  

#### Built-in Dataset: <a style="color:#006DAA;" href="https://github.com/google-research-datasets/boolean-questions">BoolQ</a>  

A dataset consisting of question-passage-answer triplets. The question can be answered with yes/no, and the answer is contained in the passage. The questions are provided anonymously and unsolicited by users of the Google search engine, and afterwards paired with a paragraph from a Wikipedia article containing the answer. We sampled 100 records out of 12697 in the full dataset.  

  

##### F1 Over Words Score  

Numerical score between 0 (worst) and 1 (best). F1-score is the harmonic mean of precision and recall. It is computed as follows:  precision = true positives / (true positives + false positives) and recall = true positives / (true positives + false negatives). Then F1 = 2 (precision * recall)/(precision + recall) .  

**Average Score: 0.10260390426566898**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Is there a sequel to the movie divergent?</td> <td style="text-align: left;"> True. The sequel to the movie divergent is Insurgent.</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.25</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is an acre bigger than a square mile?</td> <td style="text-align: left;"> False. An acre is smaller than a square mile. A square mile is 640</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.222222</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Does a d grade count as a gcse?</td> <td style="text-align: left;"> True. A D grade is a passing grade for a GCSE.

## Does a D</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.222222</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is season 7 the final season of new girl?</td> <td style="text-align: left;"> True. Season 7 is the final season of New Girl.</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.222222</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Does the unites states have an official language?</td> <td style="text-align: left;"> False. The United States does not have an official language.

## Does the United States have</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.222222</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Can you use a weaver mount on a picatinny rail?</td> <td style="text-align: left;"> False. Weaver mounts are not compatible with picatinny rails. Weaver mounts</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Do tom and hannah get together in made of honor?</td> <td style="text-align: left;"> False. Tom and Hannah do not get together in Made of Honor. Tom ends up with Melissa</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can a hiatal hernia cause an irregular heartbeat?</td> <td style="text-align: left;"> False. Hiatal hernias are not associated with irregular heartbeats. Hiatal her</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is there such a thing as maths dyslexia?</td> <td style="text-align: left;"> False. There is no such thing as maths dyslexia. Dyslexia is a</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you be an attorney without taking the bar exam?</td> <td style="text-align: left;"> False.

## Can you be an attorney without taking the bar exam?

False.</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Exact Match Score  

An exact match score is a binary score where 1 indicates the model output and answer match exactly and 0 indicates otherwise.  

**Average Score: 0.0**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Is there a jet stream in the southern hemisphere?</td> <td style="text-align: left;"> True. The southern hemisphere has a jet stream, but it is not as strong as the</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Do cows die when you tip them over?</td> <td style="text-align: left;"> False. Cows do not die when tipped over. They can be tipped over and get</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.142857</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Does forza horizon 3 work on xbox 360?</td> <td style="text-align: left;"> False. Forza Horizon 3 is only available on Xbox One and PC. The Xbox </td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you use a weaver mount on a picatinny rail?</td> <td style="text-align: left;"> False. Weaver mounts are not compatible with picatinny rails. Weaver mounts</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Was the movie papillon based on a true story?</td> <td style="text-align: left;"> True. The movie Papillon is based on the memoir of the same name by Henri Charrière</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.142857</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Is dna replication part of the central dogma?</td> <td style="text-align: left;"> True. DNA replication is the first step in the central dogma of molecular biology. The</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you mate a zebra and a horse?</td> <td style="text-align: left;"> False. Zebras and horses belong to different genera in the family Equidae and cannot inter</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you be offsides in a corner kick?</td> <td style="text-align: left;"> False. You cannot be offsides on a corner kick. The ball is not in play until</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.133333</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is molinari the first italian to win a major?</td> <td style="text-align: left;"> False. Francesco Molinari is the second Italian to win a major. The first Italian to win</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is the one and only ivan a movie?</td> <td style="text-align: left;"> True. The One and Only Ivan is a movie that was released in 2020.</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Quasi Exact Match Score  

Similar as above, but both model output and answer are normalised first by removing any articles and punctuation. E.g., 1 also for predicted answers “Antarctica.” or “the Antarctica” .  

**Average Score: 0.0**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Is there a jet stream in the southern hemisphere?</td> <td style="text-align: left;"> True. The southern hemisphere has a jet stream, but it is not as strong as the</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Do cows die when you tip them over?</td> <td style="text-align: left;"> False. Cows do not die when tipped over. They can be tipped over and get</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.142857</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Does forza horizon 3 work on xbox 360?</td> <td style="text-align: left;"> False. Forza Horizon 3 is only available on Xbox One and PC. The Xbox </td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you use a weaver mount on a picatinny rail?</td> <td style="text-align: left;"> False. Weaver mounts are not compatible with picatinny rails. Weaver mounts</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Was the movie papillon based on a true story?</td> <td style="text-align: left;"> True. The movie Papillon is based on the memoir of the same name by Henri Charrière</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.142857</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Is dna replication part of the central dogma?</td> <td style="text-align: left;"> True. DNA replication is the first step in the central dogma of molecular biology. The</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you mate a zebra and a horse?</td> <td style="text-align: left;"> False. Zebras and horses belong to different genera in the family Equidae and cannot inter</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Can you be offsides in a corner kick?</td> <td style="text-align: left;"> False. You cannot be offsides on a corner kick. The ball is not in play until</td> <td style="text-align: left;">False</td> <td style="text-align: left;">0.133333</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is molinari the first italian to win a major?</td> <td style="text-align: left;"> False. Francesco Molinari is the second Italian to win a major. The first Italian to win</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Is the one and only ivan a movie?</td> <td style="text-align: left;"> True. The One and Only Ivan is a movie that was released in 2020.</td> <td style="text-align: left;">True</td> <td style="text-align: left;">0.153846</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

#### Built-in Dataset: <a style="color:#006DAA;" href="https://github.com/google-research-datasets/natural-questions">Natural Questions</a>  

A dataset consisting of ~320K question-passage-answer triplets. The questions are factual naturally-occurring questions. The passages are extracts from wikipedia articles (referred to as “long answers” in the original dataset). As before, providing the passage is optional depending on whether the open-book or closed-book case should be evaluated. We sampled 100 records out of 4289 in the full dataset.  

  

##### F1 Over Words Score  

Numerical score between 0 (worst) and 1 (best). F1-score is the harmonic mean of precision and recall. It is computed as follows:  precision = true positives / (true positives + false positives) and recall = true positives / (true positives + false negatives). Then F1 = 2 (precision * recall)/(precision + recall) .  

**Average Score: 0.3026384448526369**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Whose new political ideas of natural rights influenced the declaration of independence?</td> <td style="text-align: left;"> John Locke</td> <td style="text-align: left;">Francis Hutcheson or John Locke</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The winner of the best actor award in the 64th national film festival 2017?</td> <td style="text-align: left;"> Akshay Kumar</td> <td style="text-align: left;">Akshay Kumar or Manoj Joshi</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The vast interior rural area of australia is known as the?</td> <td style="text-align: left;"> Outback</td> <td style="text-align: left;">The Outback or Outback</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who sings tie a yellow ribbon around the old oak tree?</td> <td style="text-align: left;"> Tony Orlando and Dawn</td> <td style="text-align: left;">Joyce Vincent Wilson or Tony Orlando and Dawn or Pamela Vincent or Motown/Stax backing vocalist Telma Hopkins</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who drove the number 18 car in nascar?</td> <td style="text-align: left;"> Kyle Busch</td> <td style="text-align: left;">Kyle Busch</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Who played g baby in the movie hardball?</td> <td style="text-align: left;"> Michael B. Jordan</td> <td style="text-align: left;">DeWayne Warren</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Who is the winner of bigg boss kannada season?</td> <td style="text-align: left;"> The winner of Bigg Boss Kannada season 1 is Vijay Raghav</td> <td style="text-align: left;">Chandan Shetty or rapper Chandan Shetty</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Who won the most medals in the 1924 winter olympics?</td> <td style="text-align: left;"> Clarence Abel of the United States won the most medals in the 192</td> <td style="text-align: left;">Norway</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >When is the new season of wentworth coming out?</td> <td style="text-align: left;"> The new season of Wentworth is set to be released on July 28, 2</td> <td style="text-align: left;">19 June 2018</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What song did the titanic band play when it sank?</td> <td style="text-align: left;"> The band played &quot;Nearer, My God, to Thee&quot; as the Titanic</td> <td style="text-align: left;">&quot;Autumn&quot;</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Exact Match Score  

An exact match score is a binary score where 1 indicates the model output and answer match exactly and 0 indicates otherwise.  

**Average Score: 0.13**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Whose new political ideas of natural rights influenced the declaration of independence?</td> <td style="text-align: left;"> John Locke</td> <td style="text-align: left;">Francis Hutcheson or John Locke</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The winner of the best actor award in the 64th national film festival 2017?</td> <td style="text-align: left;"> Akshay Kumar</td> <td style="text-align: left;">Akshay Kumar or Manoj Joshi</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The vast interior rural area of australia is known as the?</td> <td style="text-align: left;"> Outback</td> <td style="text-align: left;">The Outback or Outback</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who sings tie a yellow ribbon around the old oak tree?</td> <td style="text-align: left;"> Tony Orlando and Dawn</td> <td style="text-align: left;">Joyce Vincent Wilson or Tony Orlando and Dawn or Pamela Vincent or Motown/Stax backing vocalist Telma Hopkins</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who drove the number 18 car in nascar?</td> <td style="text-align: left;"> Kyle Busch</td> <td style="text-align: left;">Kyle Busch</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Who sings for the beast in the new movie?</td> <td style="text-align: left;"> John Newman</td> <td style="text-align: left;">Dan Stevens</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Where does wild caught sockeye salmon come from?</td> <td style="text-align: left;"> Wild caught sockeye salmon comes from the Pacific Ocean, specifically from the waters off the coast</td> <td style="text-align: left;">in the Northern Pacific Ocean and rivers discharging into it or Northern Pacific Ocean and rivers discharging into it or the Northern Pacific Ocean</td> <td style="text-align: left;">0.266667</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >When did the movie the post begin filming?</td> <td style="text-align: left;"> The Post began filming on May 30, 2017.</td> <td style="text-align: left;">May 30, 2017 or May 2017</td> <td style="text-align: left;">0.6</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What is cain and abel software used for?</td> <td style="text-align: left;"> Cain and Abel software is used for password recovery. It is a password recovery tool for Microsoft</td> <td style="text-align: left;">recover many kinds of passwords using methods such as network packet sniffing, cracking various password hashes by using methods such as dictionary attacks, brute force and cryptanalysis attacks [...]</td> <td style="text-align: left;">0.285714</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What is the concept of unfair labor practice in labor code?</td> <td style="text-align: left;"> Unfair labor practice refers to actions taken by employers or unions that violate the rights of</td> <td style="text-align: left;">in US labor law refers to certain actions taken by employers or unions that violate the National Labor Relations Act of 1935 (49 Stat. 449) 29 U.S.C. § 151–169 (also known as the NLRA and the [...]</td> <td style="text-align: left;">0.428571</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Quasi Exact Match Score  

Similar as above, but both model output and answer are normalised first by removing any articles and punctuation. E.g., 1 also for predicted answers “Antarctica.” or “the Antarctica” .  

**Average Score: 0.14**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Whose new political ideas of natural rights influenced the declaration of independence?</td> <td style="text-align: left;"> John Locke</td> <td style="text-align: left;">Francis Hutcheson or John Locke</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The winner of the best actor award in the 64th national film festival 2017?</td> <td style="text-align: left;"> Akshay Kumar</td> <td style="text-align: left;">Akshay Kumar or Manoj Joshi</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >The vast interior rural area of australia is known as the?</td> <td style="text-align: left;"> Outback</td> <td style="text-align: left;">The Outback or Outback</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who sings tie a yellow ribbon around the old oak tree?</td> <td style="text-align: left;"> Tony Orlando and Dawn</td> <td style="text-align: left;">Joyce Vincent Wilson or Tony Orlando and Dawn or Pamela Vincent or Motown/Stax backing vocalist Telma Hopkins</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Who drove the number 18 car in nascar?</td> <td style="text-align: left;"> Kyle Busch</td> <td style="text-align: left;">Kyle Busch</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Where does wild caught sockeye salmon come from?</td> <td style="text-align: left;"> Wild caught sockeye salmon comes from the Pacific Ocean, specifically from the waters off the coast</td> <td style="text-align: left;">in the Northern Pacific Ocean and rivers discharging into it or Northern Pacific Ocean and rivers discharging into it or the Northern Pacific Ocean</td> <td style="text-align: left;">0.266667</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >When did the movie the post begin filming?</td> <td style="text-align: left;"> The Post began filming on May 30, 2017.</td> <td style="text-align: left;">May 30, 2017 or May 2017</td> <td style="text-align: left;">0.6</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What is cain and abel software used for?</td> <td style="text-align: left;"> Cain and Abel software is used for password recovery. It is a password recovery tool for Microsoft</td> <td style="text-align: left;">recover many kinds of passwords using methods such as network packet sniffing, cracking various password hashes by using methods such as dictionary attacks, brute force and cryptanalysis attacks [...]</td> <td style="text-align: left;">0.285714</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What is the concept of unfair labor practice in labor code?</td> <td style="text-align: left;"> Unfair labor practice refers to actions taken by employers or unions that violate the rights of</td> <td style="text-align: left;">in US labor law refers to certain actions taken by employers or unions that violate the National Labor Relations Act of 1935 (49 Stat. 449) 29 U.S.C. § 151–169 (also known as the NLRA and the [...]</td> <td style="text-align: left;">0.428571</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >How many breeds of pigs are there in the uk?</td> <td style="text-align: left;"> There are around 14 breeds of pigs in the UK. These include the Large White</td> <td style="text-align: left;">---</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

#### Built-in Dataset: <a style="color:#006DAA;" href="http://nlp.cs.washington.edu/triviaqa/">TriviaQA</a>  

A dataset consisting of 95K question-answer pairs with with on average six supporting evidence documents per question, leading to ~650K question-passage-answer triplets. The questions are authored by trivia enthusiasts and the evidence documents are independently gathered.  We sampled 100 records out of 156328 in the full dataset.  

  

##### F1 Over Words Score  

Numerical score between 0 (worst) and 1 (best). F1-score is the harmonic mean of precision and recall. It is computed as follows:  precision = true positives / (true positives + false positives) and recall = true positives / (true positives + false negatives). Then F1 = 2 (precision * recall)/(precision + recall) .  

**Average Score: 0.5899757840199018**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >&quot;born 1888, who composed the song &quot;&quot;there&#x27;s no business like show business&quot;&quot;?&quot;?</td> <td style="text-align: left;"> Irving Berlin</td> <td style="text-align: left;">Ellin Mackay or Irving Berlin or Marie (Irving Berlin song) or I Love a Piano or Israel Baline or Israel Isadore Baline or Israel Isidore Baline or ellin mackay or marie irving berlin song or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >According to the holy bible, in order to marry which woman did king david send her husband, uriah the hittite, to meet his death in battle?</td> <td style="text-align: left;"> Bathsheba</td> <td style="text-align: left;">2 Samuel 11 or Basheva or Bathsheba at her Bath or Bath-shua or Bath-sheba or Bethsheba or Bathsheba at Bath or Bathsheba or Bathsheba at her bath or Bat Sheva or Besheba or Bathsheba at Her [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >&quot;what is the term used to describe the collection of flowers in species that have more than one flower on an axis (sometimes called &quot;&quot;composite flowers&quot;&quot;?&quot;?</td> <td style="text-align: left;"> inflorescence</td> <td style="text-align: left;">Cymes or Synflorescence or Dichasium or Flower spike or Infrutescence or Solitary flower or Scorpiod cyme or Inflorescences or Inflorescense or Interminate inflorescence or Diahcasial cyme or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >What is the name of the wizard and leader of the fellowship of the ring in tolkein’s ‘the lord of the rings’?</td> <td style="text-align: left;"> Gandalf</td> <td style="text-align: left;">Gandalf Greyhame or Greyhame or Mithrandir or Olórin or Bladorthin or Gandalf the gray or Gandalf the White or You shall not pass! or Gandlaf or Tharkun or Tharkûn or Stormcrow or Ganadalf or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Launched from cape kennedy in march 1972, what was the name of the first man-made satellite to leave the solar system?</td> <td style="text-align: left;"> Pioneer 10</td> <td style="text-align: left;">Pioneer F or 1972-012A or Pioneer 10 or 1972 012a or pioneer 10 or pioneer f or Pioneer 10 or pioneer 10</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >Which american author is mentioned in the beatles song l am the walrus?</td> <td style="text-align: left;"> Lewis Carroll</td> <td style="text-align: left;">Edgar alen poe or Edgar Allan Poe or Ea poe or Edgar allen poe or Edgar Allen Poe or The Life of Edgar Allan Poe or Poean or Poe, Edgar Allen or Edgar A. Poe or Edgar Allan Poe and the Stories [...]</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >&quot;in music, which word means &quot;&quot;lively&quot;&quot; or &quot;&quot;animated&quot;&quot;?&quot;?</td> <td style="text-align: left;"> The word in music that means &quot;lively&quot; or &quot;animated&quot; is &quot;Allegro</td> <td style="text-align: left;">Vivace or vivace or Vivace or vivace</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Who was given asses’ ears by an angry apollo?</td> <td style="text-align: left;"> Marsyas</td> <td style="text-align: left;">Midas and the Golden Touch or Touch of Midas or Midas Curse or Midas&#x27; touch or Midas Touch or King Midas or Berecynthain Hero or The Berecynthain Hero or Midas or Midas Gardens or Midas [...]</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >&quot;what product was advertised with the slogan &quot;&quot; splash it all over &quot;&quot; ?&quot;?</td> <td style="text-align: left;"> The product that was advertised with the slogan &quot;Splash it all over&quot; is the</td> <td style="text-align: left;">The Brut or BRUT or Brut or Brut (disambiguation) or brut or brut disambiguation or BRUT or brut</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >Which jockey won the derby in 1979, 1980, 1989 and 1994?</td> <td style="text-align: left;"> Steve Cauthen</td> <td style="text-align: left;">Willie Carson or WILLIE CARSON or willie carson or WILLIE CARSON or willie carson</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Exact Match Score  

An exact match score is a binary score where 1 indicates the model output and answer match exactly and 0 indicates otherwise.  

**Average Score: 0.42**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >&quot;born 1888, who composed the song &quot;&quot;there&#x27;s no business like show business&quot;&quot;?&quot;?</td> <td style="text-align: left;"> Irving Berlin</td> <td style="text-align: left;">Ellin Mackay or Irving Berlin or Marie (Irving Berlin song) or I Love a Piano or Israel Baline or Israel Isadore Baline or Israel Isidore Baline or ellin mackay or marie irving berlin song or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >According to the holy bible, in order to marry which woman did king david send her husband, uriah the hittite, to meet his death in battle?</td> <td style="text-align: left;"> Bathsheba</td> <td style="text-align: left;">2 Samuel 11 or Basheva or Bathsheba at her Bath or Bath-shua or Bath-sheba or Bethsheba or Bathsheba at Bath or Bathsheba or Bathsheba at her bath or Bat Sheva or Besheba or Bathsheba at Her [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >&quot;what is the term used to describe the collection of flowers in species that have more than one flower on an axis (sometimes called &quot;&quot;composite flowers&quot;&quot;?&quot;?</td> <td style="text-align: left;"> inflorescence</td> <td style="text-align: left;">Cymes or Synflorescence or Dichasium or Flower spike or Infrutescence or Solitary flower or Scorpiod cyme or Inflorescences or Inflorescense or Interminate inflorescence or Diahcasial cyme or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >What is the name of the wizard and leader of the fellowship of the ring in tolkein’s ‘the lord of the rings’?</td> <td style="text-align: left;"> Gandalf</td> <td style="text-align: left;">Gandalf Greyhame or Greyhame or Mithrandir or Olórin or Bladorthin or Gandalf the gray or Gandalf the White or You shall not pass! or Gandlaf or Tharkun or Tharkûn or Stormcrow or Ganadalf or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Launched from cape kennedy in march 1972, what was the name of the first man-made satellite to leave the solar system?</td> <td style="text-align: left;"> Pioneer 10</td> <td style="text-align: left;">Pioneer F or 1972-012A or Pioneer 10 or 1972 012a or pioneer 10 or pioneer f or Pioneer 10 or pioneer 10</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >What was the title of the first ever &#x27;carry on&#x27; film?</td> <td style="text-align: left;"> Carry On Sergeant (1958)</td> <td style="text-align: left;">Carry On Sergeant or Carry on Sergeant or The Dragons (Heathercrest National Service Depot) Regiment or CARRY ON SERGEANT or dragons heathercrest national service depot regiment or carry on [...]</td> <td style="text-align: left;">0.857143</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >On what day of creation did god make the sun, the moon and the stars?</td> <td style="text-align: left;"> The sun, the moon and the stars were made on the fourth day of creation.</td> <td style="text-align: left;">Independence Day (US) or Independence Day (USA) or United States Independence Day or Fourth-of-July or The Fourth of July or Fourth of July or Independence Day (United States) or July 4, 1776 or [...]</td> <td style="text-align: left;">0.285714</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >In the christian calendar, maundy falls on which day of holy week?</td> <td style="text-align: left;"> Maundy falls on Thursday of Holy Week.</td> <td style="text-align: left;">Thor&#x27;s Day or Guruvaar or Thor&#x27;s day or Thursdays or Thursday or Thurs. or Thorsday or Jupiter&#x27;s day or thorsday or thursday or thor s day or thursdays or guruvaar or thurs or [...]</td> <td style="text-align: left;">0.25</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What does the prefix &#x27;cry&#x27; mean in words such as cryogenics?</td> <td style="text-align: left;"> The prefix &#x27;cry&#x27; comes from the Greek word &#x27;kryos&#x27; which means cold or</td> <td style="text-align: left;">Coolth or Cold or Algid or Low environmental temperature or Coldest or coolth or algid or coldest or low environmental temperature or cold or Cold or cold</td> <td style="text-align: left;">0.166667</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What was beethoven&#x27;s first name?</td> <td style="text-align: left;"> Beethoven&#x27;s first name was Ludwig.</td> <td style="text-align: left;">Ludwig (disambiguation) or Ludwig or ludwig or ludwig disambiguation or Ludwig or ludwig</td> <td style="text-align: left;">0.333333</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>  

##### Quasi Exact Match Score  

Similar as above, but both model output and answer are normalised first by removing any articles and punctuation. E.g., 1 also for predicted answers “Antarctica.” or “the Antarctica” .  

**Average Score: 0.44**  

Below are a few examples of the highest and lowest-scoring examples across all categories. Some text may be truncated due to length constraints. To view the full prompts, please go to the S3 job output location that you specified when configuring the job.   

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Top 5 examples with highest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >&quot;born 1888, who composed the song &quot;&quot;there&#x27;s no business like show business&quot;&quot;?&quot;?</td> <td style="text-align: left;"> Irving Berlin</td> <td style="text-align: left;">Ellin Mackay or Irving Berlin or Marie (Irving Berlin song) or I Love a Piano or Israel Baline or Israel Isadore Baline or Israel Isidore Baline or ellin mackay or marie irving berlin song or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >According to the holy bible, in order to marry which woman did king david send her husband, uriah the hittite, to meet his death in battle?</td> <td style="text-align: left;"> Bathsheba</td> <td style="text-align: left;">2 Samuel 11 or Basheva or Bathsheba at her Bath or Bath-shua or Bath-sheba or Bethsheba or Bathsheba at Bath or Bathsheba or Bathsheba at her bath or Bat Sheva or Besheba or Bathsheba at Her [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >&quot;what is the term used to describe the collection of flowers in species that have more than one flower on an axis (sometimes called &quot;&quot;composite flowers&quot;&quot;?&quot;?</td> <td style="text-align: left;"> inflorescence</td> <td style="text-align: left;">Cymes or Synflorescence or Dichasium or Flower spike or Infrutescence or Solitary flower or Scorpiod cyme or Inflorescences or Inflorescense or Interminate inflorescence or Diahcasial cyme or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >What is the name of the wizard and leader of the fellowship of the ring in tolkein’s ‘the lord of the rings’?</td> <td style="text-align: left;"> Gandalf</td> <td style="text-align: left;">Gandalf Greyhame or Greyhame or Mithrandir or Olórin or Bladorthin or Gandalf the gray or Gandalf the White or You shall not pass! or Gandlaf or Tharkun or Tharkûn or Stormcrow or Ganadalf or [...]</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
<tr> <td >Launched from cape kennedy in march 1972, what was the name of the first man-made satellite to leave the solar system?</td> <td style="text-align: left;"> Pioneer 10</td> <td style="text-align: left;">Pioneer F or 1972-012A or Pioneer 10 or 1972 012a or pioneer 10 or pioneer f or Pioneer 10 or pioneer 10</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> <td style="text-align: left;">1.0</td> </tr>  
</table>  

<table align=center >  
<caption style="text-align: left; padding-bottom: 15px;">Bottom 5 examples with lowest scores:</caption>  
<tr> <th >Model Input</th> <th style="text-align: left;">Model Output</th> <th style="text-align: left;">Target Output</th> <th style="text-align: left;">F1 Score</th> <th style="text-align: left;">Exact Match Score</th> <th style="text-align: left;">Quasi Exact Match Score</th> </tr>  
<tr> <td >What was the title of the first ever &#x27;carry on&#x27; film?</td> <td style="text-align: left;"> Carry On Sergeant (1958)</td> <td style="text-align: left;">Carry On Sergeant or Carry on Sergeant or The Dragons (Heathercrest National Service Depot) Regiment or CARRY ON SERGEANT or dragons heathercrest national service depot regiment or carry on [...]</td> <td style="text-align: left;">0.857143</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >On what day of creation did god make the sun, the moon and the stars?</td> <td style="text-align: left;"> The sun, the moon and the stars were made on the fourth day of creation.</td> <td style="text-align: left;">Independence Day (US) or Independence Day (USA) or United States Independence Day or Fourth-of-July or The Fourth of July or Fourth of July or Independence Day (United States) or July 4, 1776 or [...]</td> <td style="text-align: left;">0.285714</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >In the christian calendar, maundy falls on which day of holy week?</td> <td style="text-align: left;"> Maundy falls on Thursday of Holy Week.</td> <td style="text-align: left;">Thor&#x27;s Day or Guruvaar or Thor&#x27;s day or Thursdays or Thursday or Thurs. or Thorsday or Jupiter&#x27;s day or thorsday or thursday or thor s day or thursdays or guruvaar or thurs or [...]</td> <td style="text-align: left;">0.25</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What does the prefix &#x27;cry&#x27; mean in words such as cryogenics?</td> <td style="text-align: left;"> The prefix &#x27;cry&#x27; comes from the Greek word &#x27;kryos&#x27; which means cold or</td> <td style="text-align: left;">Coolth or Cold or Algid or Low environmental temperature or Coldest or coolth or algid or coldest or low environmental temperature or cold or Cold or cold</td> <td style="text-align: left;">0.166667</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
<tr> <td >What was beethoven&#x27;s first name?</td> <td style="text-align: left;"> Beethoven&#x27;s first name was Ludwig.</td> <td style="text-align: left;">Ludwig (disambiguation) or Ludwig or ludwig or ludwig disambiguation or Ludwig or ludwig</td> <td style="text-align: left;">0.333333</td> <td style="text-align: left;">0.0</td> <td style="text-align: left;">0.0</td> </tr>  
</table>